// Mock Database
let posts = [];

// Post ID
let count = 1;


// Reactive DOM with JSON (CRUD Operation)

// Add post data

document.getElementById("form-add-post").addEventListener('submit', (e) => {

	// To prevent the page from reloading(default behavior of submit)
	e.preventDefault()
	posts.push({
		id: count,
		title: document.getElementById('txt-title').value,
		body: document.querySelector('#txt-body').value
	});
	count++
	console.log(posts);
	alert("Post successfully added")
	showPost();

})

// RETRIEVE POST
const showPost = () => {
	// variable that will contain all the posts
	let postEntries = "";
	// Looping through array items
	posts.forEach((post) => {
		postEntries += `
			<div id="post-${post.id}">
				<h3 id="post-title-${post.id}">${post.title}</h3>
				<p id="post-body-${post.id}">${post.body}</p>
				<button onClick="editPost('${post.id}')">Edit</button>
				<button onClick="deletePost('${post.id}')">Delete</button>
			</div
		`
	});
	console.log(postEntries);
	document.querySelector('#div-post-entries').innerHTML = postEntries

};


const editPost = (id) => {

	let title = document.querySelector(`#post-title-${id}`).innerHTML;
	let body = document.querySelector(`#post-body-${id}`).innerHTML;

	document.querySelector('#txt-edit-id').value = id;
	document.querySelector('#txt-edit-title').value = title;
	document.querySelector('#txt-edit-body').value = body;

}


// UPDATE POST
document.querySelector('#form-edit-post').addEventListener('submit', (e) => {

	e.preventDefault();

	for(let i=0; i < posts.length; i++){
		if(posts[i].id.toString() === document.querySelector('#txt-edit-id').value){
			posts[i].title = document.querySelector("#txt-edit-title").value
			posts[i].body = document.querySelector("#txt-edit-body").value

			showPost(posts);
			alert("Post successfully updated")

		}
	}

})

const deletePost = (id) => {

	for(let i=0; i< posts.length; i++){
		if(posts[i].id.toString() === id) {
			posts.splice(i,1);
		}
	}

	alert("Post successfully deleted!");
	showPost(posts);


}